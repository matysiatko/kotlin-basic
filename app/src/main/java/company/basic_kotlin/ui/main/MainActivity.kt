package company.basic_kotlin.ui.main

import android.os.Bundle
import company.basic_kotlin.App
import company.basic_kotlin.R
import company.basic_kotlin.base.BaseActivity

class MainActivity : BaseActivity(), MainView {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        App.component.inject(this)
        setContentView(R.layout.activity_main)
    }
}
