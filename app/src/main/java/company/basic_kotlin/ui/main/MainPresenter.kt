package company.basic_kotlin.ui.main

import company.basic_kotlin.base.BasePresenter
import javax.inject.Inject

/**
 * Created by Mateusz Ziomek on 15.05.17.
 */
class MainPresenter @Inject constructor(): BasePresenter<MainView>() {

    fun doIt() {

    }
}