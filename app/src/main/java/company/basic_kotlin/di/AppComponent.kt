package company.basic_kotlin.di

import company.basic_kotlin.ui.main.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Mateusz Ziomek on 15.05.17.
 */

@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun inject(mainActivity: MainActivity)
}