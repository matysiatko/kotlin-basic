package company.basic_kotlin.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Mateusz Ziomek on 16.05.17.
 */
@Module
class AppModule(private val context: Context) {

    @Provides @Singleton
    fun getContext(): Context = context
}