package company.basic_kotlin

import android.app.Application
import company.basic_kotlin.di.AppComponent
import company.basic_kotlin.di.AppModule
import company.basic_kotlin.di.DaggerAppComponent

/**
 * Created by Mateusz Ziomek on 15.05.17.
 */
class App : Application() {

    companion object Component {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}