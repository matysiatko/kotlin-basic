package company.basic_kotlin.base

import io.reactivex.disposables.CompositeDisposable
import java.lang.ref.WeakReference

/**
 * Created by Mateusz Ziomek on 15.05.17.
 */
abstract class BasePresenter<V : MvpView> : MvpPresenter<V> {

    var viewReference: WeakReference<V>? = null
    var disposables = CompositeDisposable()

    override fun bind(view: V) {
        this.viewReference = WeakReference(view)
    }

    override fun unbind() {
        this.viewReference?.clear()
        this.viewReference = null
        disposables.clear()
    }

    fun getView(): V? {
        return viewReference?.get()
    }

    fun isBound(): Boolean {
        return viewReference != null && viewReference?.get() != null
    }
}