package company.basic_kotlin.base

/**
 * Created by Mateusz Ziomek on 15.05.17.
 */
interface MvpPresenter<in V : MvpView> {

    fun bind(view: V)
    fun unbind()
}